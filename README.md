# RoadToEternity
This is the official repo for my game [Road To Eternity](https://officialduke99.itch.io/road-to-eternity). This game is made in the [Armory 3D](https://armory3d.org/) game engine.

![Banner](https://img.itch.zone/aW1nLzYwOTMyMTQucG5n/original/sJO61L.png)

## Warning:
The core of this repo is composed of mainly large binary. This means solving merge conflicts is almost impossible when changes are made to it. Only make merge requests, if you either don't do changes to the binary and only change text files or if you have my consent to change the binary. For getting my consent or for suggesting changes to the binary, open an issue.
Also, one of the assets used in the game has a CC0 Attributions license, meaning if you are to reuse it, you have to credit the creator (more details on the game's site).

## Building
Clone the repository locally. Open Blender (ver. 2.9x) with the Armory SDK (2021.6 and up) installed. 

- If you want to only run the game without creating a .exe file, then just navigate to `Render Properties - Armory Player` and hit `Play`.
- If you want to build it to a .exe file, navigate to `Render Properties - Armory Exporter` and hit `Publish`. After the process is done, you should find a `build_RoadToMain - krom-windows` folder inside of the repo. This is where the .exe ends up.
